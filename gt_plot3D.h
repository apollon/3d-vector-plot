/********************************************************************************
 **                                                                            **
 **  3D Vector Plot: A library with functions to plot 3D data using openGL     **
 **  Copyright (C) 2014 Gkratsia Tantilian                                     **
 **                                                                            **
 **  This file is part of 3D Vector Plot.                                      **
 **                                                                            **
 **  3D Vector Plot is free software: you can redistribute it and/or modify    **
 **  it under the terms of the GNU Lesser General Public License as            **
 **  published by the Free Software Foundation, either version 3 of the        **
 **  License, or (at your option) any later version.                           **
 **                                                                            **
 **  3D Vector Plot is distributed in the hope that it will be useful,         **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of            **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             **
 **  GNU Lesser General Public License for more details.                       **
 **                                                                            **
 **  You should have received a copy of the GNU Lesser General Public License  **
 **  along with 3D Vector Plot.  If not, see <http://www.gnu.org/licenses/>.   **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
 **                                                                            **
 **              Author: Gkratsia Tantilian <sciencegt@gmail.com>              **
 **          University: Aristotle University of Thessaloniki                  **
 **                Date: December 2014                                         **
 **             Version: Alpha 1.0                                             **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
 */

/* -------------------------------------------------------------------------------------------------------- */
#include <iostream>
#include <vector>
/* -------------------------------------------------------------------------------------------------------- */

class gt_point{
    public:
        double x,y,z;
        gt_point(){ x=0.0; y=0.0; z=0.0; }
};
extern int gt_plot3DVec(std::vector< std::vector<double> > vec,
        double xmin = 0, double xmax = 1, double ymin = 0, double ymax = 1,
        int argc = 0, char **argv = NULL);
extern int gt_plot2DVec(std::vector<double> x, std::vector<double> y, int is_log,
        int argc = 0, char **argv = NULL);
