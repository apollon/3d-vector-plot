3D Vector Plot
==============
Two simple C++ functions that plot the data of a 2d std::vector< std::vector<double> > for 3D plot, or
std::vector<double> for 2D plot.

*Initialization*: After preparing the model the camera is pointing to the Center of Gravity of the plot.

![![preview.png](https://bitbucket.org/repo/oerjzg/images/1703572473-preview.png)](https://bitbucket.org/repo/oerjzg/images/310426091-2v.png)

Usage
-----

*For 3D Plot*

The input data must be of type `std::vector< std::vector<double> >`.

All you have to do is include the `gt_plot3D.hpp` and call the function:

```
extern int gt_plot3DVec(std::vector< std::vector<double> > vec,
                        double xmin = 0, double xmax = 1,
                        double ymin = 0, double ymax = 1);
```

*Parameters:*

* std::vector< std::vector<double> > vec: The 2D vector with z-data
* double xmin, xmax, ymin, ymax: The domain dimensions. (optional)


```
#include "gt_plot3D.h"

int main(int argc, char **argv)
{
    int i = 0, j = 0;
    std::vector< std::vector<double> > vec(100, std::vector<double>(100, 1));
    for (std::vector< std::vector<double> >::iterator it = vec.begin(); it != vec.end(); ++it) {
        for (std::vector<double>::iterator col = it->begin(); col != it->end(); ++ col) {
            *col = 10* sin(i/20.)*cos(j/50.) + 30;
            i++;
        }
        i = 0;
        j++;
    }
    gt_plot3DVec(vec, -50,50, -50,50);
    return 0;
}
```


*For 2D Plot*

The input data must be of type `std::vector<double>`.

All you have to do is include the `gt_plot3D.hpp` and call the function:

```
extern int gt_plot2DVec(std::vector<double> x, std::vector<double> y, int is_log);
```

*Parameters:*

* std::vector<double> x: The x-coordinate
* std::vector<double> y: The y-coordinate
* int is\_log: Define plot type (0: Cartesian, 1: Natural Logarithmic - base = e, 10: Logarithmic - base=10  )

*Note*
The vectors x and y must have the same size.



```
#include "gt_plot3D.h"

int main(int argc, char **argv)
{
    std::vector<double> x;
    std::vector<double> y;
    for (int i = -100; i < 100; ++i) {
        x.push_back((double) i/10.);
        y.push_back(10*sin((double) i/10.));
    }
    gt_plot2DVec(x, y, 0);

    return 0;
}
```


Examples (included in `main.c`)
------------------------------

To run the example:

* Download the source code
* Change Directory into the path that contains the source code
* run `qmake`
* run `make`
* run `./gt_plot3D`

*NOTE:* To switch between 3D and 2D examples set PLOT_3D macro in the file main.c to 1 or 0 respectively.


Controls
--------

Key                   | Functionality
---------------------:|:-------------
Ctrl + left click     | Rotate camera
Shift + left click    | Translate camera
Scroll weel           | Zoom In / Out
H                     | Reset perspective (if model out of perspective)
R                     | Reset Camera position
F5                    | Toggle Full Screen
ESC                   | Quit
A (or a)              | Toggle axes view
G (or g)              | Toggle grid view
F (or f)              | Toggle fill mode (not supported yet)
L (or l)              | Toggle line (only for 2D plot)


Dependencies
------------

* qmake
* openGL
* GLUT

License
-------

GNU Lesser General Public License
