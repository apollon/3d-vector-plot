/********************************************************************************
 **                                                                            **
 **  3D Vector Plot: A library with functions to plot 3D data using openGL     **
 **  Copyright (C) 2014 Gkratsia Tantilian                                     **
 **                                                                            **
 **  This file is part of 3D Vector Plot.                                      **
 **                                                                            **
 **  3D Vector Plot is free software: you can redistribute it and/or modify    **
 **  it under the terms of the GNU Lesser General Public License as            **
 **  published by the Free Software Foundation, either version 3 of the        **
 **  License, or (at your option) any later version.                           **
 **                                                                            **
 **  3D Vector Plot is distributed in the hope that it will be useful,         **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of            **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             **
 **  GNU Lesser General Public License for more details.                       **
 **                                                                            **
 **  You should have received a copy of the GNU Lesser General Public License  **
 **  along with 3D Vector Plot.  If not, see <http://www.gnu.org/licenses/>.   **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
 **                                                                            **
 **              Author: Gkratsia Tantilian <sciencegt@gmail.com>              **
 **          University: Aristotle University of Thessaloniki                  **
 **                Date: December 2014                                         **
 **             Version: Alpha 1.0                                             **
 **                                                                            **
 **  ------------------------------------------------------------------------  **
 */

/* -------------------------------------------------------------------------------------------------------- */
/* INCLUDE HEADERS */
#include "gt_plot3D.h"
#include <GL/glut.h>
#include <cmath>
#include <algorithm>
#include <stdio.h>
/* -------------------------------------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------------------------------------------------ */
/* FORWARD DECLARATIONS */
static void updateViewPort(void);
/* ------------------------------------------------------------------------------------------------------------------ */

/* ------------------------------------------------------------------------------------------------------------------ */
/* CLASSES AND MACROS */
class GLPoint {
public:
    GLdouble x, y, z;
    GLPoint() { x = 30.; y = 30.; z = 3.;}
};

#define crossProduct(a,b,c) \
	(a)[0] = (b)[1] * (c)[2] - (c)[1] * (b)[2]; \
	(a)[1] = (b)[2] * (c)[0] - (c)[2] * (b)[0]; \
	(a)[2] = (b)[0] * (c)[1] - (c)[0] * (b)[1];;
/* ------------------------------------------------------------------------------------------------------------------ */

/* ------------------------------------------------------------------------------------------------------------------ */
/* GLOBAL VARIABLES */
GLPoint CamPos;
GLPoint trgtView;
std::vector<gt_point> pointVector;
gt_point    MODEL_COG;
gt_point    MODEL_SIZE;

bool    FULLSCREEN = false;
int     SHOW_AXES = 1;
int     SHOW_GRID = 1;
int     SHOW_LINE = 1;
float   AXIS_SIZE = 10.0;
float   POINT_SIZE = 0.2;
float   UP_DIR = 1.0;
int     MOVE_ACTIVE = 0;
int     glPolyMode = GL_FILL;
float   dAngleX = 0.0, angleX = M_PI/4.0;
float   dAngleY = 0.0, angleY = M_PI / 4.0;
int     x0_mouse = -1;
int     y0_mouse = -1;
int     x0_mouse_tr = -1;
int     y0_mouse_tr = -1;
double  defCamX = 10., defCamY = 10., defCamZ = 10.;
double  defTrgX = 0., defTrgY = 0., defTrgZ = 0.;

double  GT_ZOOM = 1.5;
double  MOVE_X = 0.0, MOVE_Y = 0.0;

bool    PLOT_3D = true;
bool    IS_LOG = false;
/* ------------------------------------------------------------------------------------------------------------------ */

/* ------------------------------------------------------------------------------------------------------------------ */
/* STATIC FUNCTIONS */
static void normalize(double vec[3])
{
    double r = sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2]);
    vec[0] *= 1.0/r;
    vec[1] *= 1.0/r;
    vec[2] *= 1.0/r;
}

static void drawText(char *text, GLfloat x, GLfloat y, GLfloat z)
{
    char *p;

    glRasterPos3f(x, y, z);

    for (p = text; *p; p++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *p);
    }
}

static void drawText2D(char *text, GLfloat x, GLfloat y)
{
    drawText(text, x, y, 0.1);
}

static void Arrow(GLdouble x1,GLdouble y1,GLdouble z1,GLdouble x2,GLdouble y2,GLdouble z2,GLdouble D)
{
    double x=x2-x1;
    double y=y2-y1;
    double z=z2-z1;
    double L=sqrt(x*x+y*y+z*z);
    double RADPERDEG = 0.0174533;

    GLUquadricObj *quadObj;

    glPushMatrix();

    glTranslated(x1,y1,z1);

    if((x!=0.)||(y!=0.)) {
        glRotated(atan2(y,x)/RADPERDEG,0.,0.,1.);
        glRotated(atan2(sqrt(x*x+y*y),z)/RADPERDEG,0.,1.,0.);
    } else if (z<0){
        glRotated(180,1.,0.,0.);
    }

    glTranslatef(0,0,L-5*D);

    quadObj = gluNewQuadric();
    gluQuadricDrawStyle(quadObj, GLU_FILL);
    gluQuadricNormals(quadObj, GLU_SMOOTH);
    gluCylinder(quadObj, 2*D, 0.0, 5*D, 32, 1);
    gluDeleteQuadric(quadObj);

    quadObj = gluNewQuadric();
    gluQuadricDrawStyle (quadObj, GLU_FILL);
    gluQuadricNormals (quadObj, GLU_SMOOTH);
    gluDisk(quadObj, 0.0, 2*D, 32, 1);
    gluDeleteQuadric(quadObj);

    glTranslatef(0,0,-L+5*D);

    quadObj = gluNewQuadric();
    gluQuadricDrawStyle(quadObj, GLU_FILL);
    gluQuadricNormals(quadObj, GLU_SMOOTH);
    gluCylinder(quadObj, D, D, L-5*D, 32,1);
    gluDeleteQuadric(quadObj);

    quadObj = gluNewQuadric();
    gluQuadricDrawStyle(quadObj, GLU_FILL);
    gluQuadricNormals(quadObj, GLU_SMOOTH);
    gluDisk(quadObj, 0.0, D, 32, 1);
    gluDeleteQuadric(quadObj);

    glPopMatrix();

}

static void drawAxes(GLdouble length)
{
    float   radius = length / 30.;
    double  viewport = 1.2 * length;
    char    txt_x[2], txt_y[2], txt_z[2];

    sprintf(txt_x, "X");
    sprintf(txt_y, "Y");
    sprintf(txt_z, "Z");

    glOrtho(-viewport, viewport,
            -viewport, viewport,
            -viewport, viewport);

    glRotated( - angleY * (180 / M_PI) ,1.,0.,0.);
    glRotated( - angleX * (180 / M_PI) ,0.,0.,1.);

    glColor3f(1.0f, 0.0f, 0.0f);
    Arrow(0,0,0, length,0,0, radius);
    drawText(txt_x, 1.2 * length, 0.0, 0.0);

    glColor3f(0.0f, 1.0f, 0.0f);
    Arrow(0,0,0, 0, length,0, radius);
    drawText(txt_y, 0.0, 1.2 * length, 0.0);

    glColor3f(0.0f, 0.0f, 1.0f);
    Arrow(0,0,0, 0,0, length, radius);
    drawText(txt_z, 0.0, 0.0, 1.2 * length);

    glColor3f(1.0f, 1.0f, 1.0f);
    glutWireSphere(length/10.0, 20, 5);

    glColor3f(0.0f, 0.5f, 0.5f);
}

static void DrawAxes(void)
{
    if (SHOW_AXES) {
        int axesViewport = 80;
        int w = glutGet(GLUT_WINDOW_WIDTH);
        int h = glutGet(GLUT_WINDOW_HEIGHT);

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glViewport(10, h - 10 - axesViewport, axesViewport, axesViewport);
        glPushMatrix();
        glLoadIdentity();
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();

        drawAxes(AXIS_SIZE);

        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
        glPolygonMode(GL_FRONT_AND_BACK, glPolyMode);

        glViewport(0, 0, w, h);
    }
}

static void showGrid(GLdouble axisLength)
{
    glColor3f(0.8f, 0.8f, 0.8f);
    glPushMatrix();
    glBegin(GL_QUADS);
    for (int i = -5; i < 6; ++i) {
        for (int j = -5; j < 6; ++j) {
            glVertex3f(0,0,0);
            glVertex3f(axisLength*i ,0,0);
            glVertex3f(axisLength*i,axisLength*j,0);
            glVertex3f(0,axisLength*j,0);
        }
    }
    glEnd();
    glPopMatrix();
    glColor3f(0.0f, 0.5f, 0.5f);
}

static void DrawGrid(void)
{
    if (SHOW_GRID) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        showGrid(AXIS_SIZE);
        glPolygonMode(GL_FRONT_AND_BACK, glPolyMode);
    }
}

static void DrawAxes2D(void)
{
    if (SHOW_AXES) {
        char    txt_x[2], txt_y[2];
        int     max_dim = (MODEL_SIZE.x > MODEL_SIZE.y) ? MODEL_SIZE.x : MODEL_SIZE.y;
        double  xmin = (IS_LOG) ? 0.0 : MODEL_COG.x - max_dim;
        double  ymin = (IS_LOG) ? 0.0 : MODEL_COG.y - max_dim;

        max_dim = ceil(max_dim);

        sprintf(txt_x, "X");
        sprintf(txt_y, "Y");

        glLineWidth(2.0);
        glColor3f(0.0f, 0.0f, 0.0f);

        glColor3f(1.0f, 0.0f, 0.0f);
        drawText(txt_x, ceil(MODEL_COG.x + max_dim), 0.0, 0.0);
        glBegin(GL_LINES);
        glVertex3f(xmin, 0.0, 0.0);
        glVertex3f(ceil(MODEL_COG.x + max_dim), 0.0, 0.0);
        glEnd();

        glColor3f(0.0f, 1.0f, 0.0f);
        drawText(txt_y, 0.0, ceil(MODEL_COG.y + max_dim), 0.0);
        glBegin(GL_LINES);
        glVertex3f(0.0, ymin, 0.0);
        glVertex3f(0.0, ceil(MODEL_COG.y + max_dim), 0.0);
        glEnd();
        glLineWidth(1.0);
    }
}

static void DrawGrid2D(void)
{
    if (SHOW_GRID) {
        glColor3f(0.3,0.3,0.3);
        glLineStipple(2, 0xAAAA);
        glEnable(GL_LINE_STIPPLE);

        double  max_dim = (MODEL_SIZE.x > MODEL_SIZE.y) ? MODEL_SIZE.x : MODEL_SIZE.y;
        char    i_s[24];

        max_dim = ceil(max_dim);

        if (IS_LOG) {
            for (double i = 0.; i < ceil(MODEL_COG.x + max_dim) + max_dim / 10.; i += max_dim / 10.) {
                glBegin(GL_LINES);
                glVertex3f(i, 0.0, 0.0);
                glVertex3f(i, ceil(MODEL_COG.y + max_dim), 0.0);
                glEnd();
                sprintf(i_s, "%.2lf", i);
                drawText2D(i_s, i - max_dim / 40., -max_dim / 10.0);
            }
            for (double i = 1.0; i < ceil(MODEL_COG.y + max_dim) + max_dim / 10.; i *= 10.) {
                glBegin(GL_LINES);
                glVertex3f(0., i, 0.0);
                glVertex3f(ceil(MODEL_COG.x + max_dim), i, 0.0);
                glEnd();
                sprintf(i_s, "%.2lf", i);
                drawText2D(i_s, -max_dim / 10.0, i - max_dim / 40.);
            }
        } else {
            for (double i = floor(MODEL_COG.x - max_dim); i < ceil(MODEL_COG.x + max_dim) + max_dim / 10.; i += max_dim / 10.) {
                glBegin(GL_LINES);
                glVertex3f(i, floor(MODEL_COG.y - max_dim), 0.0);
                glVertex3f(i, ceil(MODEL_COG.y + max_dim), 0.0);
                glEnd();
                sprintf(i_s, "%.2lf", i);
                drawText2D(i_s, i - max_dim / 40., -max_dim / 10.0);
            }
            for (double i = floor(MODEL_COG.y - max_dim); i < ceil(MODEL_COG.y + max_dim) + max_dim / 10.; i += max_dim / 10.) {
                glBegin(GL_LINES);
                glVertex3f(floor(MODEL_COG.x - max_dim), i, 0.0);
                glVertex3f(ceil(MODEL_COG.x + max_dim), i, 0.0);
                glEnd();
                sprintf(i_s, "%.2lf", i);
                drawText2D(i_s, -max_dim / 10.0, i - max_dim / 40.);
            }
        }
        glDisable(GL_LINE_STIPPLE);
    }
}

static void DrawPoints(std::vector<gt_point> vec)
{
    for (std::vector< gt_point >::iterator it = vec.begin(); it != vec.end(); ++it) {
        glBegin(GL_POINTS);
        glVertex3f((*it).x,(*it).y,(*it).z);
        glEnd();
    }
} /* DrawPoints */

static bool pointCmp(gt_point a, gt_point b) { return (a.x < b.x); }

static void DrawPoints2D(std::vector<gt_point> vec)
{
    glColor3f(0.2, 0.2, 0.8);
    std::sort(vec.begin(), vec.end(), pointCmp);

    for (std::vector< gt_point >::iterator it = vec.begin(); it != vec.end(); ++it) {
        glBegin(GL_POINTS);
        glVertex3f((*it).x,(*it).y, 0.1);
        glEnd();
    }
    if (SHOW_LINE) {
        glBegin(GL_LINE_STRIP);
        for (std::vector< gt_point >::iterator it = vec.begin(); it != vec.end(); ++it) {
            glVertex3f((*it).x,(*it).y, 0.1);
        }
        glEnd();
    }
} /* DrawPoints2D */

static float calcNewDepth(void)
{
    return 2 * sqrt(pow(MODEL_COG.x - CamPos.x, 2) + pow(MODEL_COG.y - CamPos.y, 2) + pow(MODEL_COG.z - CamPos.z, 2));
}

static void updateViewPort(void)
{
    int w = glutGet(GLUT_WINDOW_WIDTH);
    int h = glutGet(GLUT_WINDOW_HEIGHT);
    if (h == 0) h = 1;
    float ratio = w * 1.0 / h;
    float depth = calcNewDepth();

    // Use the Projection Matrix
    glMatrixMode(GL_PROJECTION);
    // Reset Matrix
    glLoadIdentity();
    // Set the correct perspective.
    gluPerspective(45.0f, ratio, 0.1f, depth);
    // Get Back to the Modelview
    glMatrixMode(GL_MODELVIEW);
}

static void display3D(void)
{
    // Set Polygon Mode
    glPolygonMode(GL_FRONT_AND_BACK, glPolyMode);
    // Clear Color and Depth Buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    // Set the camera
    gluLookAt(
            CamPos.x, CamPos.y, CamPos.z,
            trgtView.x, trgtView.y, trgtView.z,
            0.0f, 0.0f, UP_DIR * 1.0f);

    DrawPoints(pointVector);
    DrawAxes();
    DrawGrid();
}

static void display2D(void)
{
    double max_dim = (MODEL_SIZE.x > MODEL_SIZE.y) ? MODEL_SIZE.x : MODEL_SIZE.y;
    max_dim = ceil(max_dim);

    // Set Polygon Mode
    glPolygonMode(GL_FRONT_AND_BACK, glPolyMode);
    // Clear Color and Depth Buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    glOrtho(MODEL_COG.x - max_dim * GT_ZOOM + MOVE_X,
            MODEL_COG.x + max_dim * GT_ZOOM + MOVE_X,
            MODEL_COG.y - max_dim * GT_ZOOM + MOVE_Y,
            MODEL_COG.y + max_dim * GT_ZOOM + MOVE_Y,
            0.01, 1000);

    DrawPoints2D(pointVector);
    DrawAxes2D();
    DrawGrid2D();
}

static void Display(void)
{
    if ( PLOT_3D ) {
        display3D();
    } else {
        display2D();
    }

    glFlush();
    glutSwapBuffers();
}

static void Resize(int w, int h)
{
    if (h == 0) h = 1;
    float ratio = w * 1.0 / h;
    float depth = calcNewDepth();

    // Use the Projection Matrix
    glMatrixMode(GL_PROJECTION);
    // Reset Matrix
    glLoadIdentity();
    // Set the viewport to be the entire window
    glViewport(0, 0, w, h);
    // Set the correct perspective.
    gluPerspective(45.0f, ratio, 0.1f, depth);
    // Get Back to the Modelview
    glMatrixMode(GL_MODELVIEW);
}

static void Idle(void)
{
    ; /* Nothing yet */
}

static void Keyboard(unsigned char key, int x, int y)
{
    switch(key)
    {
        case 27:
            exit(EXIT_SUCCESS);
            break;
        case 'A':
        case 'a':
            SHOW_AXES = !SHOW_AXES;
            glutPostRedisplay(); // call display
            break;
        case 'G':
        case 'g':
            SHOW_GRID = !SHOW_GRID;
            glutPostRedisplay(); // call display
            break;
        case 'F':
        case 'f':
            glPolyMode = (glPolyMode == GL_FILL) ? GL_LINE : GL_FILL;
            glutPostRedisplay(); // call display
            break;
        case 'R':
        case 'r':
            CamPos.x = defCamX;
            CamPos.y = defCamY;
            CamPos.z = defCamZ;
            trgtView.x = defTrgX;
            trgtView.y = defTrgY;
            trgtView.z = defTrgZ;
            angleX = angleY = M_PI / 4.0;
            glutPostRedisplay(); // call display
            break;
        case 'H':
        case 'h':
            updateViewPort();
            glutPostRedisplay(); // call display
            break;
        case 'L':
        case 'l':
            SHOW_LINE = (SHOW_LINE) ? 0 : 1;
            glutPostRedisplay(); // call display
            break;
    }
}

static void toggleFullscreen(void)
{
    if (!FULLSCREEN) {
        glutFullScreen();
        FULLSCREEN = true;
    } else {
        glutPositionWindow(100,100);
        glutReshapeWindow(800,600);
        FULLSCREEN = false;
    }
    glutPostRedisplay(); // call display
}

static void SpecialKeyPress(int key, int x, int y)
{
    switch (key) {
        case GLUT_KEY_UP:
            break;
        case GLUT_KEY_DOWN:
            break;
        case GLUT_KEY_F5:
            toggleFullscreen();
            break;
    }
}

static void SpecialKeyRelease(int key, int x, int y)
{
    switch (key) {
        case GLUT_KEY_UP:
        case GLUT_KEY_DOWN:
            break;
    }
}

static void Mouse(int button, int state, int x, int y)
{
    if (button == GLUT_LEFT_BUTTON && glutGetModifiers() == GLUT_ACTIVE_CTRL) {
        if (state == GLUT_DOWN) {
            x0_mouse = x;
            y0_mouse = y;
        }
    } else {
        x0_mouse = -1;
        y0_mouse = -1;
    }

    if (button == GLUT_LEFT_BUTTON && glutGetModifiers() == GLUT_ACTIVE_SHIFT) {
        if (state == GLUT_DOWN) {
            x0_mouse_tr = x;
            y0_mouse_tr = y;
        }
    } else {
        x0_mouse_tr = -1;
        y0_mouse_tr = -1;
    }

    if (PLOT_3D) {
        if (button == 3) { /* weel up */
            double rxyz = sqrt(
                    (CamPos.x - trgtView.x) * (CamPos.x - trgtView.x) +
                    (CamPos.y - trgtView.y) * (CamPos.y - trgtView.y) +
                    (CamPos.z - trgtView.z) * (CamPos.z - trgtView.z)
                    );

            double new_r = rxyz + 2.;
            CamPos.x = trgtView.x + new_r * sin(angleY) * cos(angleX);
            CamPos.y = trgtView.y + new_r * sin(angleY) * sin(angleX);
            CamPos.z = trgtView.z + new_r * cos(angleY);
            glutPostRedisplay(); // call display
        }
        if (button == 4) { /* weel down */
            double rxyz = sqrt(
                    (CamPos.x - trgtView.x) * (CamPos.x - trgtView.x) +
                    (CamPos.y - trgtView.y) * (CamPos.y - trgtView.y) +
                    (CamPos.z - trgtView.z) * (CamPos.z - trgtView.z)
                    );

            double new_r = (rxyz > AXIS_SIZE) ?  rxyz - 2. : rxyz;
            CamPos.x = trgtView.x + new_r * sin(angleY) * cos(angleX);
            CamPos.y = trgtView.y + new_r * sin(angleY) * sin(angleX);
            CamPos.z = trgtView.z + new_r * cos(angleY);
            glutPostRedisplay(); // call display
        }
    } else {
        if (button == 3) { /* weel up */
            if (GT_ZOOM - 0.1 > 0.0) GT_ZOOM -= 0.1;
            glutPostRedisplay(); // call display
        }
        if (button == 4) { /* weel down */
            GT_ZOOM += 0.1;
            glutPostRedisplay(); // call display
        }
    }
}

static void dragMouse(int x, int y)
{
    if (PLOT_3D) {
        double rxyz = sqrt(
                (CamPos.x - trgtView.x) * (CamPos.x - trgtView.x) +
                (CamPos.y - trgtView.y) * (CamPos.y - trgtView.y) +
                (CamPos.z - trgtView.z) * (CamPos.z - trgtView.z)
                );
        double rxy = rxyz * sin(angleY);
        if (x0_mouse >= 0) {
            dAngleX = (x - x0_mouse) * 0.001f;
            if (UP_DIR > 0) {
                angleX -= dAngleX;
            } else {
                angleX += dAngleX;
            }
            angleX = fmod(angleX, 2.0 * M_PI);
            CamPos.x = trgtView.x + rxy * cos(angleX);
            CamPos.y = trgtView.y + rxy * sin(angleX);
            x0_mouse = x;
        }
        if (y0_mouse >= 0) {
            dAngleY = (y - y0_mouse) * 0.001f;
            angleY -= dAngleY;
            angleY = fmod(angleY, 2.0 * M_PI);
            CamPos.x = trgtView.x + rxy * cos(angleX);
            CamPos.y = trgtView.y + rxy * sin(angleX);
            CamPos.z = trgtView.z + rxyz * cos(angleY);
            y0_mouse = y;
            if ( (angleY <= 0 && angleY >= - M_PI) || (angleY >= M_PI && angleY < 2.0 * M_PI) ) {
                UP_DIR = -1.0;
            } else {
                UP_DIR = 1.0;
            }
        }

        if (x0_mouse_tr >= 0) {
            double Nr[3], Nx[3];
            double Nz[3] = {0,0,1};
            Nr[0] = CamPos.x - trgtView.x;
            Nr[1] = CamPos.y - trgtView.y;
            Nr[2] = CamPos.z - trgtView.z;
            normalize(Nr);
            crossProduct(Nx, Nr, Nz);
            normalize(Nx);

            if (UP_DIR > 0) {
                CamPos.x += (x - x0_mouse_tr) * Nx[0] * 0.1;
                CamPos.y += (x - x0_mouse_tr) * Nx[1] * 0.1;
                CamPos.z += (x - x0_mouse_tr) * Nx[2] * 0.1;

                trgtView.x += (x - x0_mouse_tr) * Nx[0] * 0.1;
                trgtView.y += (x - x0_mouse_tr) * Nx[1] * 0.1;
                trgtView.z += (x - x0_mouse_tr) * Nx[2] * 0.1;
            } else {
                CamPos.x -= (x - x0_mouse_tr) * Nx[0] * 0.1;
                CamPos.y -= (x - x0_mouse_tr) * Nx[1] * 0.1;
                CamPos.z -= (x - x0_mouse_tr) * Nx[2] * 0.1;

                trgtView.x -= (x - x0_mouse_tr) * Nx[0] * 0.1;
                trgtView.y -= (x - x0_mouse_tr) * Nx[1] * 0.1;
                trgtView.z -= (x - x0_mouse_tr) * Nx[2] * 0.1;
            }

            x0_mouse_tr = x;
        }
        if (y0_mouse_tr >= 0) {
            double Nr[3], Nx[3], Ny[3];
            double Nz[3] = {0,0,1};
            Nr[0] = CamPos.x - trgtView.x;
            Nr[1] = CamPos.y - trgtView.y;
            Nr[2] = CamPos.z - trgtView.z;
            normalize(Nr);
            crossProduct(Nx, Nr, Nz);
            normalize(Nx);
            crossProduct(Ny, Nx, Nr);
            normalize(Ny);

            CamPos.x += (y - y0_mouse_tr) * Ny[0] * 0.1;
            CamPos.y += (y - y0_mouse_tr) * Ny[1] * 0.1;
            CamPos.z += (y - y0_mouse_tr) * Ny[2] * 0.1;

            trgtView.x += (y - y0_mouse_tr) * Ny[0] * 0.1;
            trgtView.y += (y - y0_mouse_tr) * Ny[1] * 0.1;
            trgtView.z += (y - y0_mouse_tr) * Ny[2] * 0.1;

            y0_mouse_tr = y;
        }
    } else {
        double max_dim = (MODEL_SIZE.x > MODEL_SIZE.y) ? MODEL_SIZE.x : MODEL_SIZE.y;
        max_dim = ceil(max_dim);
        double width = 2.0 * max_dim * GT_ZOOM;
        int w = glutGet(GLUT_WINDOW_WIDTH);
        int h = glutGet(GLUT_WINDOW_HEIGHT);

        if (x0_mouse_tr >= 0) {
            MOVE_X -= (x - x0_mouse_tr) * (width / (2.0 * w));
            x0_mouse_tr = x;
        }
        if (y0_mouse_tr >= 0) {
            MOVE_Y += (y - y0_mouse_tr) * (width / (2.0 * h));
            y0_mouse_tr = y;
        }
    }

    glutPostRedisplay(); // call display
}

static void rightClickMenu(int item)
{
    switch (item) {
        case 1:
            toggleFullscreen();
            break;
        case 2:
            exit(EXIT_SUCCESS);
            break;
    }
    glutPostRedisplay(); // call display
}

static void createRightClickMenu(void)
{
    glutCreateMenu(rightClickMenu);
    glutAddMenuEntry("Toggle fullscreen (F5)", 1);
    glutAddMenuEntry("Quit (ESC)", 2);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

static std::vector<gt_point> dblvec2gtpvec(std::vector< std::vector<double> > vec, double xmin, double xmax, double ymin, double ymax)
{
    std::vector<gt_point> ret_vec;
    double x = xmin, y = ymin;
    double dx = (xmax - xmin) / ((double) vec.at(0).size());
    double dy = (ymax - ymin) / ((double) vec.size());
    for (std::vector< std::vector<double> >::iterator it = vec.begin(); it != vec.end(); ++it) {
        for (std::vector<double>::iterator col = it->begin(); col != it->end(); ++ col) {
            gt_point    point3D;
            point3D.x = x;
            point3D.y = y;
            point3D.z = *col;
            ret_vec.push_back(point3D);
            x += dx;
        }
        x = xmin;
        y += dy;
    }
    return ret_vec;
}

static std::vector<gt_point> dblvec2gtpvec2D(std::vector<double> x, std::vector<double> y, int is_log)
{
    std::vector<gt_point> ret_vec;
    if (is_log == 1) {
        for (unsigned int i = 0; i < x.size(); ++i) {
            gt_point    point3D;
            point3D.x = x.at(i);
            point3D.y = log(y.at(i));
            ret_vec.push_back(point3D);
        }
    } else if (is_log == 10) {
        for (unsigned int i = 0; i < x.size(); ++i) {
            gt_point    point3D;
            point3D.x = x.at(i);
            point3D.y = log10(y.at(i));
            ret_vec.push_back(point3D);
        }
    } else {
        for (unsigned int i = 0; i < x.size(); ++i) {
            gt_point    point3D;
            point3D.x = x.at(i);
            point3D.y = y.at(i);
            ret_vec.push_back(point3D);
        }
    }
    return ret_vec;
}

static void InitView(std::vector< std::vector<double> > vec,
        double xmin, double xmax, double ymin, double ymax)
{
    if (vec.size() == 0) {
        trgtView.x = 0.0;
        trgtView.y = 0.0;
        trgtView.z = 0.0;

        MODEL_SIZE.x = 0.0;
        MODEL_SIZE.y = 0.0;
        MODEL_SIZE.z = 0.0;

        MODEL_COG.x = 0.0;
        MODEL_COG.y = 0.0;
        MODEL_COG.z = 0.0;
    } else {
        double zmin, zmax;
        int i, j;
        zmin = zmax = vec.at(0).at(0);

        for (std::vector< std::vector<double> >::iterator it = vec.begin(); it != vec.end(); ++it) {
            for (std::vector<double>::iterator col = it->begin(); col != it->end(); ++ col) {
                if (zmin > *col) zmin = *col;
                if (zmax < *col) zmax = *col;
                i++;
            }
            i = 0;
            j++;
        }

        defTrgX = trgtView.x = MODEL_COG.x = (xmax + xmin) / 2.0;
        defTrgY = trgtView.y = MODEL_COG.y = (ymax + ymin) / 2.0;
        defTrgZ = trgtView.z = MODEL_COG.z = (zmax + zmin) / 2.0;

        MODEL_SIZE.x = xmax - xmin;
        MODEL_SIZE.y = ymax - ymin;
        MODEL_SIZE.z = zmax - zmin;

        CamPos.x = MODEL_COG.x + MODEL_SIZE.x;
        CamPos.y = MODEL_COG.y + MODEL_SIZE.y;
        CamPos.z = MODEL_COG.z + MODEL_SIZE.z;
    }

    double rxyz = sqrt(
            (CamPos.x - trgtView.x) * (CamPos.x - trgtView.x) +
            (CamPos.y - trgtView.y) * (CamPos.y - trgtView.y) +
            (CamPos.z - trgtView.z) * (CamPos.z - trgtView.z)
            );
    double rxy = rxyz * sin(angleY);
    defCamX = CamPos.x = trgtView.x + rxy * cos(angleX);
    defCamY = CamPos.y = trgtView.y + rxy * sin(angleX);
    defCamZ = CamPos.z = trgtView.z + rxyz * cos(angleX);
    int maxModelSize = (std::max(MODEL_SIZE.x, MODEL_SIZE.y) > MODEL_SIZE.z) ? std::max(MODEL_SIZE.x, MODEL_SIZE.z) : MODEL_SIZE.z;
    AXIS_SIZE = (maxModelSize + maxModelSize / 2.0) / 10.0;
    POINT_SIZE = 5.;
}

static void InitView2D(std::vector<double> x, std::vector<double> y, int is_log)
{
    if (x.size() == 0 && y.size() == 0) {
        MODEL_SIZE.x = 0;
        MODEL_SIZE.y = 0;
        MODEL_SIZE.z = 0;

        MODEL_COG.x = 0;
        MODEL_COG.y = 0;
        MODEL_COG.z = 0;
    } else {
        double xmin, xmax;
        double ymin, ymax;

        xmin = xmax = x.at(0);
        ymin = ymax = y.at(0);

        for (std::vector<double>::iterator it = x.begin(); it != x.end(); ++it) {
            if (xmin > *it) xmin = *it;
            if (xmax < *it) xmax = *it;
        }

        if (is_log == 1) {
            for (std::vector<double>::iterator it = y.begin(); it != y.end(); ++it) {
                if (ymin > log(*it)) ymin = log(*it);
                if (ymax < log(*it)) ymax = log(*it);
            }
        } else if (is_log == 10) {
            for (std::vector<double>::iterator it = y.begin(); it != y.end(); ++it) {
                if (ymin > log10(*it)) ymin = log10(*it);
                if (ymax < log10(*it)) ymax = log10(*it);
            }
        } else {
            for (std::vector<double>::iterator it = y.begin(); it != y.end(); ++it) {
                if (ymin > *it) ymin = *it;
                if (ymax < *it) ymax = *it;
            }
        }

        MODEL_SIZE.x = xmax - xmin;
        MODEL_SIZE.y = ymax - ymin;
        MODEL_SIZE.z = 0;

        MODEL_COG.x = (xmax - xmin) / 2.0;
        MODEL_COG.y = (ymax - ymin) / 2.0;
        MODEL_COG.z = 0;
    }
    POINT_SIZE = 5.;
}

static void InitGL(void) {
    if (PLOT_3D) {
        glClearColor(0.3,0.3,0.3,1.0);
    } else {
        glClearColor(0.7,0.7,0.7,1.0);
    }
    glutSetCursor(GLUT_CURSOR_CROSSHAIR);
    Display();
}

/* ------------------------------------------------------------------------------------------------------------------ */

/* ------------------------------------------------------------------------------------------------------------------ */
/* EXTERNAL FUNXTIONS */
extern int gt_plot3DVec(std::vector< std::vector<double> > vec,
        double xmin, double xmax, double ymin, double ymax,
        int argc, char **argv)
{
    InitView(vec, xmin,xmax, ymin,ymax);
    pointVector = dblvec2gtpvec(vec, xmin, xmax, ymin, ymax);

    //Initialize GLUT
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB | GLUT_MULTISAMPLE );
    glutInitWindowPosition(100,100);
    glutInitWindowSize(800,600);
    updateViewPort();

    //Create a window
    glutCreateWindow("3D Vector Plot");
    glPolygonMode(GL_FRONT_AND_BACK, glPolyMode);

    //Enable the vertex array functionality
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    // Set point size and bg color
    glPointSize(POINT_SIZE);

    // Run Initial Setup
    InitGL();

    // Load callback functions
    glutDisplayFunc(Display);
    glutReshapeFunc(Resize);
    glutKeyboardFunc(Keyboard);
    glutSpecialFunc(SpecialKeyPress);
    glutSpecialUpFunc(SpecialKeyRelease);
    glutMotionFunc(dragMouse);
    glutMouseFunc(Mouse);
    glutIdleFunc(Idle);
    // Right Click Menu
    createRightClickMenu();

    glEnable(GL_DEPTH_TEST);

    glutMainLoop();
    return 0;
}

extern int gt_plot2DVec(std::vector<double> x, std::vector<double> y, int is_log,
        int argc, char **argv)
{
    if (x.size() != y.size()) return 0;

    PLOT_3D = false;
    if (is_log) IS_LOG = true;

    InitView2D(x, y, is_log);
    pointVector = dblvec2gtpvec2D(x, y, is_log);

    //Initialize GLUT
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB | GLUT_MULTISAMPLE );
    glutInitWindowPosition(100,100);
    glutInitWindowSize(800,600);
    updateViewPort();

    char title[32];
    sprintf(title, "%s", (is_log == 1) ? "2D Vector Plot - Log" :
            (is_log == 10) ? "2D Vector Plot - Log10" : "2D Vector Plot");

    //Create a window
    glutCreateWindow(title);

    //Enable the vertex array functionality
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    // Set point size and bg color
    glPointSize(POINT_SIZE);

    // Run Initial Setup
    InitGL();

    // Load callback functions
    glutDisplayFunc(Display);
    glutReshapeFunc(Resize);
    glutKeyboardFunc(Keyboard);
    glutSpecialFunc(SpecialKeyPress);
    glutSpecialUpFunc(SpecialKeyRelease);
    glutMotionFunc(dragMouse);
    glutMouseFunc(Mouse);
    glutIdleFunc(Idle);
    // Right Click Menu
    createRightClickMenu();

    glEnable(GL_DEPTH_TEST);

    glutMainLoop();
    return 0;
}

/* ------------------------------------------------------------------------------------------------------------------ */

