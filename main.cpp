#include "gt_plot3D.h"
#include <cmath>

#define PLOT_3D 1
int main(int argc, char **argv)
{
#if PLOT_3D
    int i = 0, j = 0;
    std::vector< std::vector<double> > vec(100, std::vector<double>(100, 1));
    for (std::vector< std::vector<double> >::iterator it = vec.begin(); it != vec.end(); ++it) {
        for (std::vector<double>::iterator col = it->begin(); col != it->end(); ++ col) {
            *col = 10* sin(i/20.)*cos(j/50.) + 30;
            i++;
        }
        i = 0;
        j++;
    }
    gt_plot3DVec(vec, -50,50, -50,50);
#else
    int is_log = 10;
    std::vector<double> x;
    std::vector<double> y;
    for (double i = 1.; i < 10.; i += 1.0) {
        x.push_back(i);

        //y.push_back(10*log10((double) i/10.));
        //y.push_back(10*sin(i/10.));
        //y.push_back(exp(i));
        y.push_back(pow(10.0, i));
    }
    gt_plot2DVec(x, y, is_log);
#endif
    return 0;
}
