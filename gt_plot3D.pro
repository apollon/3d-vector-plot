######################################################################
# Automatically generated by qmake (2.01a) Sun Dec 14 13:00:54 2014
######################################################################

TEMPLATE = app
TARGET = gt_plot3D
DEPENDPATH += .
INCLUDEPATH += .
LIBS += -lm -lGL -L/usr/X11R6/lib -lGLU -lglut
QMAKE_CXXFLAGS += -Wno-unused-parameter
CONFIG += silent

# Input
HEADERS += gt_plot3D.h
SOURCES += gt_plot3D.cpp main.cpp


